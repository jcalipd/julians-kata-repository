package pencil_durability_kata;

public class Pencil {

	private String paper;

	private int durability = 100;

	private int textLength;

	private int substringLength;

	private int initialDurability = 50;

	private int pencilLength = 10;

	private int erasedTextIndex;

	private int erasedTextLength;

	private int eraserDurability = 10;

	public String write(String paper, String newText) {

		textLength = newText.length();
		substringLength = textLength;

		for (int i = 0; i < newText.length(); i++) {

			char[] charArray = newText.toCharArray();
			String[] newTextArray = newText.split("");
			if (Character.isUpperCase(charArray[i])) {
				textLength++;
				substringLength--;
			}
			if (Character.isWhitespace(charArray[i])) {
				textLength--;
				substringLength--;
			}

		}
		durability = durability - textLength;
		if (textLength > durability) {

			newText = newText.substring(0, substringLength);

			return paper + newText;
		}

		return paper + newText;
	}

	public void sharpen() {
		if (pencilLength > 0) {

			durability = initialDurability;
			pencilLength--;
		}
	}

	public String erase(String paper, String eraseText) {

		if (eraserDurability < eraseText.length()) {

			String newEraseText = eraseText.substring(eraseText.length() - eraserDurability);

			paper = paper.substring(0, paper.lastIndexOf(newEraseText)) + makeBlanks(newEraseText)
					+ paper.substring(paper.lastIndexOf(newEraseText) + newEraseText.length());

			erasedTextLength = newEraseText.length();
			return paper;
		}
		
		paper = paper.substring(0, paper.lastIndexOf(eraseText)) + makeBlanks(eraseText)
				+ paper.substring(paper.lastIndexOf(eraseText) + eraseText.length());

		erasedTextLength = eraseText.length();
		return paper;
	}
	public String edit(String paper, String newText) {
		int whitespaceBeginningIndex = paper.indexOf("  ") + 1;
		paper = paper.substring(0, whitespaceBeginningIndex) +
				newText + paper.substring(whitespaceBeginningIndex + newText.length());
		
		return paper;
	}

	private String makeBlanks(String eraseText) {
		String blanks = "";
		for (int i = 0; i < eraseText.length(); i++) {
			blanks = blanks + " ";
		}
		return blanks;
	}

	// getters and setters
	public String getPaper() {
		return paper;
	}

	public void setPaper(String paper) {
		this.paper = paper;
	}

	public int getDurability() {
		return durability;
	}

	public void setDurability(int durability) {
		this.durability = durability;
	}

	public int getTextLength() {
		return textLength;
	}

	public void setTextLength(int textLength) {
		this.textLength = textLength;
	}

	public int getSubstringLength() {
		return substringLength;
	}

	public void setSubstringLength(int substringLength) {
		this.substringLength = substringLength;
	}

	public int getInitialDurability() {
		return initialDurability;
	}

	public void setInitialDurability(int initialDurability) {
		this.initialDurability = initialDurability;
	}

	public int getPencilLength() {
		return pencilLength;
	}

	public void setPencilLength(int pencilLength) {
		this.pencilLength = pencilLength;
	}

	public int getErasedTextIndex() {
		return erasedTextIndex;
	}

	public void setErasedTextIndex(int erasedTextIndex) {
		this.erasedTextIndex = erasedTextIndex;
	}

	public int getErasedTextLength() {
		return erasedTextLength;
	}

	public void setErasedTextLength(int erasedTextLength) {
		this.erasedTextLength = erasedTextLength;
	}

	public int getEraserDurability() {
		return eraserDurability;
	}

	public void setEraserDurability(int eraserDurability) {
		this.eraserDurability = eraserDurability;
	}

}
