package pencil_durability_kata;

import org.junit.*;


public class PencilTest {

	private Pencil pencil;
	private String paper;
	private String newText;
	
	@Before
	public void setUp() {
		pencil = new Pencil();
		paper = "She sells sea shells";
		newText = " by the sea shore";

	}
	//Write
	@Test
	public void writeMethodAddsTextToPaperString() {

		Assert.assertEquals("She sells sea shells by the sea shore", pencil.write(paper, newText));

	}
	//Point Degradation
	@Test 
	public void lowercaseLettersDevalueDurabilityByOne() {
		newText = "text";
		pencil.setDurability(4);
		pencil.write("",newText);
		
		Assert.assertEquals(0, pencil.getDurability());

	}
	@Test
	public  void uppercaseLettersDevalueDurabilityByTwo() {
		newText="Text";
		pencil.setDurability(5);
		pencil.write("",newText);
		
		Assert.assertEquals(0, pencil.getDurability());
		
	}
	@Test 
	public void spacesDoNotDevalueDurability() {
		newText="tex t";
		pencil.setDurability(5);
		pencil.write("",newText);
		
		Assert.assertEquals(1,pencil.getDurability());
		
	}
	@Test 
	public void newLinesDoNotDevalueDurability() {
		newText="text\n\n\n";
		pencil.setDurability(5);
		pencil.write("",newText);
		
		Assert.assertEquals(1,pencil.getDurability());
		
	}
	@Test
	public void pencilWritesuntilDurabilityIsZero() {
		newText = "Text";
		pencil.setDurability(4);
		
		Assert.assertEquals("Tex", pencil.write("", newText));
	}
	//Sharpen
	@Test
	public void sharpenMethodRestoresDurability() {
		pencil.setDurability(5);
		pencil.setInitialDurability(5);
		pencil.write("", "text");
		pencil.sharpen();
		Assert.assertEquals(5, pencil.getDurability());
	}
	@Test
	public void sharpenMethodReducesLengthByOne() {
		pencil.setPencilLength(4);
		pencil.sharpen();
		Assert.assertEquals(3, pencil.getPencilLength());
	}
	@Test
	public void sharpenDoesNotRestoreDurabilityIfPencilLengthIsZero() {
		pencil.setInitialDurability(5);
		pencil.setDurability(0);
		pencil.setPencilLength(0);
		pencil.sharpen();
		Assert.assertEquals(0, pencil.getDurability());
	}
	//Erase
	@Test
	public void eraseReplacesLastOccurenceOfTextWithWhiteSpacesOfSameLength() {
		
		String paper = "How much wood would a woodchuck chuck if a woodchuck could chuck wood?";
		String eraseText = "chuck";
		Assert.assertEquals("How much wood would a woodchuck chuck if a woodchuck could       wood?", pencil.erase(paper, eraseText));
		
	}
	//Eraser Degradation
	@Test
	public void eraseMethodCeasesWhenDurabilityIsZero() {
		String paper = "Buffalo Bill";
		String eraseText = "Buffalo Bill";
		pencil.setEraserDurability(3);
		Assert.assertEquals("Buffalo B   ", pencil.erase(paper, eraseText));
	}
	//Editing
	@Test
	public void editMethodInsertsNewTextInWhiteSpaceIfItFits() {
		String paper = "An       a day keeps the doctor away";
	
		
		Assert.assertEquals("An onion a day keeps the doctor away", pencil.edit(paper, "onion"));
		
	}
	@Test
	public void editMethodProducesSpecialCharacterWhenCharacterCollisionsOccur() {
		String paper = "An       a day keeps the doctor away";
		pencil.setErasedTextLength(5);
		
	 	Assert.assertEquals("An artich@k@ay keeps the doctor away", pencil.edit(paper, "artichoke"));
	}

}
