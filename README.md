This pencil durability application includes 13 JUnit tests that preceded the development of each function. 
Each test is inspired by the user story in the kata. 
I made this in Eclipse.
Every test can be run at once by right-clicking the PencilTest.java file, scrolling down the fly-out menu to Run As> and
selecting Junit Test from a second fly-out menu. 
Enjoy!